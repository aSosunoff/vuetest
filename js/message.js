/* version 2.1 */
; (function (win) {
    var mainBlock = $('<div>')
            .attr('id', 'alertContent')
            .css({
                'position': 'fixed',
                'top': '25px',
                'right': '25px',
                'width': '400px',
                'z-index': '1100'
            }),
        messageAlertName = "message-alert";

    $('body').append(mainBlock);

    function show(text, alertType, delay) {
        delay = delay || 2000;

        var countElement = $("[id^='" + messageAlertName + "']").length;

        var unicNameClass = messageAlertName + countElement;

        mainBlock.append("<div id='" + unicNameClass + "' class='alert " + alertType + "' style='display: none'>" + text + "</dib>");

        var alert = $("#" + unicNameClass);

        alert.fadeIn(
            () => setTimeout(() => alert.fadeOut(() => alert.remove()), delay)
        );
    };

    var deferredMessage = [];

    var _library = function() {
        var obj = {
            info: (text, delay) => { show(text, 'alert-info', delay); },
            danger: (text, delay) => { show(text, 'alert-danger', delay); },
            success: (text, delay) => { show(text, 'alert-success', delay); },
            warning: (text, delay) => { show(text, 'alert-warning', delay); },
            push: (text, type) => {
                deferredMessage.push({
                    text: text,
                    type: type
                });
            },
            run: (delay) => {
                deferredMessage.forEach((e) => {
                    switch (e.type) {
                        case 'info': obj.info(e.text, delay); break;
                        case 'danger': obj.danger(e.text, delay); break;
                        case 'success': obj.success(e.text, delay); break;
                        case 'warning': obj.warning(e.text, delay); break;
                        default: obj.info(e.text, delay);
                    }
                });
                deferredMessage = [];
            }
        };

        return obj;
    };

    win.Message = new _library();
})(this);