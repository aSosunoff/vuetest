Vue.component('text-field', {
    data: function(){
        return {
            value: '',
        }
    },
    props: ['id', 'label', 'placeholder'],
    template: `
        <div class="form-group">
            <label :for="id">{{label}}</label>
            <input 
                class="form-control" 
                v-model="value"
                :id="id" 
                :placeholder="placeholder"/>
        </div>`
});

Vue.component('number-field', {
    data: function(){
        return {
            value: null
        }
    },
    props: ['id', 'label', 'placeholder', 'type'],
    template: `
        <div class="form-group">
            <label :for="id">{{label}}</label>
            <input 
                class="form-control" 
                v-model="value"
                :id="id" 
                :placeholder="placeholder"
                type="number"/>
        </div>`
});

Vue.component('btn-danger', {
    props: ['text'],
    template: `<div class="btn btn-danger" v-on:click="onClick">{{text}}</div>`,
    methods: {
        onClick: function(){
            this.$emit('onclick');
        }
    }
});

Vue.component('btn-primary', {
    props: ['text'],
    template: `<div class="btn btn-primary" v-on:click="onClick">{{text}}</div>`,
    methods: {
        onClick: function(){
            this.$emit('onclick');
        }
    }
});