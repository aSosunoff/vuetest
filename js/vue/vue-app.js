//https://ru.vuejs.org/v2/guide/instance.html
//https://ru.vuejs.org/v2/guide/components.html
//https://ru.vuejs.org/v2/api/#%D0%9E%D0%BF%D1%86%D0%B8%D0%B8-%E2%80%94-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5

//https://ru.vuejs.org/v2/guide/syntax.html
//https://metanit.com/web/vuejs/1.7.php

Vue.component('table-column', {
    props: ['name_column', 'data_index'],
    template: `<th scope="col">{{name_column}}</th>`
});

Vue.component('table-panel', {
    props: {
        data: { defualt: () => [], type: [Array, Function] }
    },
    data: function(){
        return {
            collumns: [],
            rows: [],
            allSlected: false,
        }
    },
    methods: {
        thRender: function(){

            if(this.data.length > 0){
                let result = '';

                this.$slots.default
                    .filter(column => column.tag)
                    .reduce((a, c) => {
                        //debugger;
                        if(c.data.attrs && c.data.attrs.column_name)
                            result += `<th scope="col">${c.data.attrs.column_name}</th>`;
                        else
                            result += `<th scope="col"></th>`;
                    }, '');

                return result;
            }
        }
    },
    template: `
        <table class="table table-striped table-bordered table-hover">
            <thead class="thead-dark">
                <tr v-html="thRender()">
                    <!--<th scope="col">-->
                        <!--<input type="checkbox" v-model="allSlected">-->
                    <!--</th>-->
                    <!--<th scope="col">№</th>-->
                    <!--<th scope="col">Имя</th>-->
                    <!--<th scope="col">Возраст</th>-->
                    <!--<th scope="col"></th>-->
                </tr>
            </thead>
            <tbody>
                <tr
                    v-for="item in data"
                    v-bind:class="{'table-primary': item.isSelected, 'table-success': item.isOneSelect }"
                    v-on:click="oneSelect(item)">

                    <td><input type="checkbox" v-model="item.isSelected"></td>
                    <td>{{item.id}}</td>
                    <td>{{item.name}}</td>
                    <td>{{item.age}}</td>
                    <td>
                        <div class="btn btn-danger" v-on:click="removeById(item.id)">Удалить</div>
                    </td>
                </tr>
            </tbody>
        </table>`
});

let table = new Vue({
    el: '#table',
    data: {
        list: [
            {id: 1, name: 'Александр', age: '25', isSelected: false, isOneSelect: false},
            {id: 2, name: 'Игорь', age: '18', isSelected: false, isOneSelect: false},
            {id: 3, name: 'Владимир', age: '29', isSelected: false, isOneSelect: false},
        ]
    },
    methods: {
        oneSelect: function(item){
            this.list.forEach((e) => {
                if(e.id != item.id)
                    e.isOneSelect = false;
            });
            if(!item.isOneSelect){
                this.$refs.name.value = item.name;
                this.$refs.age.value = item.age;
            } else {
                this.$refs.name.value = '';
                this.$refs.age.value = null;
            }
            item.isOneSelect = !item.isOneSelect;

        }
    }
});

let app = new Vue({
    el: '#app',
    data: {
        list: [
            {id: 1, name: 'Александр', age: '25', isSelected: false, isOneSelect: false},
            {id: 2, name: 'Игорь', age: '18', isSelected: false, isOneSelect: false},
            {id: 3, name: 'Владимир', age: '29', isSelected: false, isOneSelect: false},
            {id: 4, name: 'Рик', age: '56', isSelected: false, isOneSelect: false},
            {id: 5, name: 'Дэвид', age: '33', isSelected: false, isOneSelect: false},
            {id: 6, name: 'Владимир', age: '54', isSelected: false, isOneSelect: false},
            {id: 7, name: 'Александр', age: '23', isSelected: false, isOneSelect: false},
        ],
        allSlected: false,
        limitItemForPage: 5,
        currectPage: 1,
        showPageNumber: 3
    },
    watch: {
        allSlected: function(newValue, oldValue){
            this.visibleList.forEach((e) => {
                e.isSelected = newValue;
            });
        },
        list: function(newValue){
            if(!newValue.length)
                this.allSlected = false;
            if(this.countPage() < this.currectPage && this.currectPage > 1)
                this.currectPage--;
        }
    },
    computed: {
        visibleList: function(){
            let start = 0,
                end = this.limitItemForPage;

            if(this.currectPage > 1){
                end = this.currectPage * this.limitItemForPage;
                start = end - this.limitItemForPage;
            }

            return this.list.slice(start, end);
        },
    },
    methods: {
        pageNumbers: function(){
            let arPageNumber = [];

            for(let i = 1; (i <= this.showPageNumber && this.countPage() >= i); i++){
                arPageNumber.push(i);
            }
            return arPageNumber;
        },
        countPage: function(){
            return Math.ceil(this.list.length / this.limitItemForPage);
        },
        added: function(){
            let id = this.list.length + 1;
            this.list.push({
                id: id,
                name: this.$refs.name.value,
                age: this.$refs.age.value,
                isSelected: false,
                isOneSelect: false
            });
            Message.success('Добавлен новый пользователь');
        },
        removeById: function(id){
            let element = this.list.find(item => item.id == id);
            let index = this.list.indexOf(element);
            this.list.splice(index, 1);
            Message.info(`Удалён элемент: ${element.id}`);
        },
        removeSelected: function(){
            let vm = this;
            let removedElements = vm.list.filter((e) => { return e.isSelected; });

            if(!removedElements.length) {
                Message.warning("Элементы не выбраны");
                return;
            }

            removedElements.forEach((e) => { vm.removeById(e.id); });
            this.allSlected = false;
        },
        oneSelect: function(item){
            this.list.forEach((e) => {
                if(e.id != item.id)
                    e.isOneSelect = false;
            });
            if(!item.isOneSelect){
                this.$refs.name.value = item.name;
                this.$refs.age.value = item.age;
            } else {
                this.$refs.name.value = '';
                this.$refs.age.value = null;
            }
            item.isOneSelect = !item.isOneSelect;

        }
    }
});